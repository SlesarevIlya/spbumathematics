const fs = require("fs");

const cleanFile = () => {
    if (fs.existsSync("output.out"))
        fs.unlinkSync("output.out");
};

const getGraphFromString = (file) => {

    return getGraph(file);
};

const getGraphFromFile = (fileName) => {
    let file = fs.readFileSync(fileName, "utf8");

    return getGraph(file);
};

const getGraph = (file) => {
    file = file.split("\n");
    const graph = [];
    for (const line of file) {
        const node = line.split(":");
        if (parseInt(node[0]) === 5) {
            let a = 5;
        }

        graph.push({
            value: parseInt(node[0]),
            nodes: node[1] !== " " ? node[1].substring(1).split(" ").map(Number).sort(
                (a, b) => a - b) : [],
            visited: false
        })
    }

    graph.sort((a, b) => a.value - b.value);

    return graph;
};

const getTasks1 = (fileName) => {
    let file = fs.readFileSync(fileName, "utf8");
    file = file.split("\n");

    const tasks = [];
    let taskLines = [];
    let taskCounter = 0;

    for (let i = 1; i < file.length; i++) {
        if (file[i][0] === "#") {
            tasks[taskCounter] = taskLines;
            taskCounter++;
            taskLines = [];
        } else {
            taskLines.push(file[i]);
        }
    }
    tasks[taskCounter] = taskLines.join("\n");

    return tasks;
};

const getTasks2 = (fileName) => {
    let file = fs.readFileSync(fileName, "utf8");
    file = file.split("\n");

    const tasks = [];
    let taskLines = [];
    let taskCounter = 0;

    for (let i = 1; i < file.length; i++) {
        if (file[i][0] === "#") {
            if (taskCounter === 0) {
                tasks[taskCounter] = taskLines.join("\n");
            } else {
                tasks[taskCounter] = taskLines;
            }
            taskCounter++;
            taskLines = [];
        } else {
            taskLines.push(file[i]);
        }
    }
    tasks[taskCounter] = taskLines;

    return tasks;
};

const getTasks3 = (fileName) => {
    let file = fs.readFileSync(fileName, "utf8");
    file = file.split("\n");

    const tasks = [];
    let taskLines = [];
    let taskCounter = 0;

    for (let i = 1; i < file.length; i++) {
        if (file[i][0] === "#") {
            tasks[taskCounter] = taskLines;
            taskCounter++;
            taskLines = [];
        } else {
            taskLines.push(file[i]);
        }
    }
    tasks[taskCounter] = taskLines;

    return tasks;
};

const writeToFile = (line) => {
    fs.appendFile("output.out", line, (err) => {
        if (err)
            return console.log(err);
    });
};

module.exports.getGraphFromFile = getGraphFromFile;
module.exports.getGraphFromString = getGraphFromString;
module.exports.getTasks1 = getTasks1;
module.exports.getTasks2 = getTasks2;
module.exports.getTasks3 = getTasks3;
module.exports.writeToFile = writeToFile;
module.exports.cleanFile = cleanFile;

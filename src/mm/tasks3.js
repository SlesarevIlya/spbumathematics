const {getGraphFromFile, getTasks3, writeToFile, cleanFile} = require("./io");

let graph;
let tasks;
let sets = [];

const getFirstAnswer = () => {
    const answer = [];
    let maxLength = 0;

    //1 condition
    if (sets[0].length !== 0) {
        answer.push(-1);
    } else {
        loop: for (let i = 1; i < sets.length; i++) {
            for (const elem of sets[i]) {
                if (!graph.find(element => element.value === elem)) {
                    if (!answer.includes(-1)) answer.push(-1);
                    break loop;
                }
            }
        }
    }

    //2,3,4 conditions
    for (let i = 0; i < sets.length - 1; i++) {
        const diff = getDifference(sets[i], sets[i + 1]);
        //2 condition
        if (diff.find(element => element.status === 1) &&
            diff.find(element => element.status === 0)) {
            if (!answer.includes(-2)) answer.push(-2);
        }
        //3 condition
        let addAmount = 0;
        for (const elem of diff) {
            if (elem.status === 1) addAmount++;
        }
        if (addAmount > 1) answer.push(-3);
        if (sets[i].length > maxLength) maxLength = sets[i].length;
    }
    if (sets[sets.length - 1].length > maxLength) maxLength = sets[sets.length - 1].length;

    if (answer.length === 0) {
        return maxLength;
    }
    return answer.join(", ");
};

const getSecondAnswer = () => {
    let A = sets[0];
    for (let i = 1; i < sets.length; i++) {
        A = mergeSets(A, sets[i]);
    }

    if (isEqualArrays(A.sort(), getNodes().sort())) {
        return "Y";
    }

    return printGraph(getInnerGraph(A));
};

const getThirdAnswer = () => {
    for (let i = 0; i < sets.length - 1; i++) {
        const diff = getDifference(sets[i], sets[i + 1]);
        if ((diff.length !== 0) && (diff.find(element => element.status === 0))) {
            return 0;
        }
    }

    return 1;
};

const isEqualArrays = (array1, array2) => {
    if (array1.length !== array2.length) return false;
    for (let i = 0; i < array1.length; i++) {
        if (array1[i] !== array2[i]) return false;
    }
    return true;
};

const getDifference = (set1, set2) => {
    const difference = [];
    for (const elem of set2) {
        if (!set1.includes(elem)) {
            difference.push({
                value: elem,
                status: 1
            })
        }
    }
    for (const elem of set1) {
        if (!set2.includes(elem)) {
            difference.push({
                value: elem,
                status: 0
            })
        }
    }
    return difference;
};

const getInnerGraph = (set) => {
    const innerGraph = [];

    for (const node of set) {
        const adjNodes = [];
        const nodeIndex = searchNodeIndex(graph, node);
        for (const adjNode of graph[nodeIndex].nodes) {
            if (set.includes(adjNode)) {
                adjNodes.push(adjNode);
            }
        }

        innerGraph.push({
            value: node,
            nodes: adjNodes.sort((a, b) => a - b)
        })
    }

    return innerGraph;
};

const getNodes = () => {
    const nodes = [];

    for (const node of graph) {
        nodes.push(node.value);
    }

    return nodes;
};

//set1 == A, set2 == Z
const mergeSets = (set1, set2) => {
    const mergedSet = set2;

    for (const elem of set1) {
        if (!set2.includes(elem)) {
            let flagToPush = true;
            const elemIndex = searchNodeIndex(graph, elem);
            //adjNode === neighbor
            for (const adjNode of graph[elemIndex].nodes) {
                if (!set1.includes(adjNode)) {
                    flagToPush = false;
                }
            }

            if (flagToPush) {
                mergedSet.push(elem);
            }
        }
    }
    return mergedSet.sort((a, b) => a - b);
};

const printGraph = (graph) => {
    let printLine = "";

    for (const node of graph) {
        printLine += `${node.value}: ${node.nodes.join(" ")} \n`;
    }

    return printLine;
};

const searchNodeIndex = (graph, nodeNumber) => {
    for (let i = 0; i < graph.length; i++) {
        if (graph[i].value === nodeNumber) {
            return i;
        }
    }
    return null;
};

const startTasks3 = () => {
    cleanFile();

    tasks = getTasks3("test3.in");
    for (const set of tasks[0]) {
        const elems = set.substring(1, set.length - 1).split(", ");
        if (elems[0].length !== 0) {
            sets.push(elems.map(Number).sort((a, b) => a - b));
        } else {
            sets.push([]);
        }
    }
    graph = getGraphFromFile("test3.graph");
    writeToFile("#3.1\n" + getFirstAnswer() + "\n#3.2\n" + getSecondAnswer() + "#3.3\n" + getThirdAnswer());
};

module.exports.startTasks3 = startTasks3;
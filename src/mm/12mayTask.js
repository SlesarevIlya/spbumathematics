const xi = (t, d, l, u) => {
    return Math.max(l, Math.min(t / d, u));
};

const g = (t, d, l, u, b) => {
    let res = -b;
    for (let i = 0; i < d.length(); i++) {
        res += xi(t, d[i], l[i], u[i]);
    }
    return res;
};

const find_root = (lx, rx, d, l, u, b) => {
    const ly = g(lx, d, l, u, b);
    const ry = g(rx, d, l, u, b);
    return lx - ly * (rx - lx) / (ry - ly);
};

// BinSearch Method
const bin_solve = (d, l, u, b) => {
    let n = d.length();
    let t = [];
    for (let i = 0; i < n; i++) {
        t.push(d[i] * l[i]);
        t.push(d[i] * u[i]);
    }
    t.sort();
    let lo = 0;
    let hi = t.length();
    while (hi - lo > 1) {
        let mid = (lo + hi) / 2;
        if (g(t[mid], d, l, u, b) <= 0) {
            lo = mid;
        } else {
            hi = mid;
        }
    }
    return find_root(t[lo], t[hi], d, l, u, b);
};

// Recurrent Method
const rec_solve = (d, l, u, b) => {
    let n = d.size();
    let t = [];
    for (let i = 0; i < n; i++) {
        t.push({
            first: d[i] * l[i],
            second: 1 / d[i],
        });
        t.push({
            first: d[i] * u[i],
            second: -1 / d[i],
        });
    }
    t.sort((f, s) => f.first - s.first);
    let val = g(t[0].first, d, l, u, b);
    let k = 0;
    let i = 0;
    while (i < t.length()) {
        val += k * (i ? t[i].first - t[i - 1].first : 0);
        if (val >= 0) {
            break;
        }
        k += t[i].second;
        i++;
    }

    return find_root(t[i - 1].first, t[i].first, d, l, u, b);
};

const main = () => {
    let n, d, l, u, b;
    let root1 = bin_solve(d, l, u, b);
    let root2 = rec_solve(d, l, u, b);
};

main();
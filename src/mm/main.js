const {startTasks1} = require("./tasks1");
const {startTasks2} = require("./tasks2");
const {startTasks3} = require("./tasks3");

const main = () => {
    //startTasks1();
    //startTasks2();
    startTasks3();
};

main();

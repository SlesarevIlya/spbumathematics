const {getGraphFromFile, getTasks2, writeToFile, cleanFile} = require("./io");

let graph;
let tasks;

const getFirstAnswer = () => {
     const sets = tasks[0].split("\n");

     const lineToSet = (line) => line.split(" ").map(Number).sort((a, b) => a - b);
     const Z = lineToSet(sets[0]);
     const A = lineToSet(sets[1]);

     return mergeSets(A, Z);
};

const getSecondAnswer = (A1) => {

    return getInnerGraph(A1);
};

const getThirdAnswer = (A1) => {
    const Z = tasks[0].split("\n")[0].split(" ").map(Number);
    Z.push(Number(tasks[2]));
    const Z1 = Z.sort((a, b) => a - b);
    const A2 = mergeSets(A1, Z1);
    const H = getInnerGraph(A2);

    let isSubgraph = true;
    for (let node of H) {
        const nodeIndex = searchNodeIndex(graph, node.value);
        if (!checkAdjNodes(graph[nodeIndex].nodes, node.nodes)) {
            isSubgraph = false;
            break;
        }
    }

    const graphBorder = new Set();
    if (isSubgraph) {
        for (const node of graph) {
            const nodeIndexInH = searchNodeIndex(H, node.value);
            if (nodeIndexInH !== null) {
                for (const element of node.nodes) {
                    if (!H[nodeIndexInH].nodes.includes(element)) {
                        graphBorder.add(H[nodeIndexInH].value);
                    }
                }
            }
        }

        return Array.from(graphBorder).join(" ");
    }

    return "It is not subgraph";
};

const getFourthAnswer = (A1) => {
    const deleteNode = Number(tasks[3]);
    const Z = tasks[0].split("\n")[0].split(" ").map(Number);
    Z.push(Number(tasks[2]));
    const Z1 = Z.sort((a, b) => a - b);
    const Z2 = Z1.filter(node => node !== deleteNode);
    let A2 = mergeSets(A1, Z1);

    let deleteNodeFromA = false;
    for (let node of graph[searchNodeIndex(graph, deleteNode)].nodes) {
        if (!A2.includes(node)) {
            deleteNodeFromA = true;
        }
    }
    if (deleteNodeFromA) {
        A2 = A2.filter(node => node !== deleteNode);
    }

    const B = mergeSets(A2, Z2);
    let edges = "";
    for (let i = 0; i < B.length; i++) {
        let children = graph[searchNodeIndex(graph, B[i])].nodes;
        for (let child of children) {
            if (B.slice(i, B.length).includes(child)) {
                edges += `\n${B[i]}-${child}`;
            }
        }
    }

    return edges;
};

const checkAdjNodes = (nodes1, nodes2) => {
    for (const node of nodes2) {
        if (!nodes1.includes(node)) {
            return false;
        }
    }

    return true;
};

const getInnerGraph = (set) => {
    const innerGraph = [];

    for (const node of set) {
        const adjNodes = [];
        const nodeIndex = searchNodeIndex(graph, node);
        for (const adjNode of graph[nodeIndex].nodes) {
            if (set.includes(adjNode)) {
                adjNodes.push(adjNode);
            }
        }

        innerGraph.push({
            value: node,
            nodes: adjNodes.sort((a, b) => a - b)
        })
    }

    return innerGraph;
};

//set1 == A, set2 == Z
const mergeSets = (set1, set2) => {
    const mergedSet = set2;

    for (const elem of set1) {
        if (!set2.includes(elem)) {
            let flagToPush = true;
            const elemIndex = searchNodeIndex(graph, elem);
            //adjNode === neighbor
            for (const adjNode of graph[elemIndex].nodes) {
                if (!set1.includes(adjNode)) {
                    flagToPush = false;
                }
            }

            if (flagToPush) {
                mergedSet.push(elem);
            }
        }
    }

    return mergedSet.sort((a, b) => a - b);
};

const searchNodeIndex = (graph, nodeNumber) => {
    for (let i = 0; i < graph.length; i++) {
        if (graph[i].value === nodeNumber) {
            return i;
        }
    }

    return null;
};

const startTasks2 = () => {
    cleanFile();

    tasks = getTasks2("test2.in");
    graph = getGraphFromFile("test2.graph");

    const A1 = getFirstAnswer();
    const G2 = getSecondAnswer(A1);
    const task3 = getThirdAnswer(A1);
    const task4 = getFourthAnswer(A1);

    writeToFile("#2.1" + "\n" + A1.join(" ") + "\n" +
        "#2.2" + "\n"  + printGraph(G2) +
        "#2.3" + "\n"  + task3 + "\n" +
        "#2.4" + task4);
};

const printGraph = (graph) => {
    let printLine = "";

    for (const node of graph) {
        printLine += `${node.value}: ${node.nodes.join(" ")} \n`;
    }

    return printLine;
};

module.exports.startTasks2 = startTasks2;

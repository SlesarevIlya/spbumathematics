const {getGraphFromString, getGraphFromFile, getTasks1, writeToFile, cleanFile} = require("./io");
const {stripIndent} = require('common-tags');

let graph;
let tasks;

const getFirstAnswer = () => {
    const nodeNumber = parseInt(tasks[0]);

    return graph[searchNodeIndex(graph, nodeNumber)].nodes.join(" ");
};

const getSecondAnswer = () => {
    const DFS = (graph, nodeNumber) => {
        graph[searchNodeIndex(graph, nodeNumber)].visited = true;
        for (const link of graph[searchNodeIndex(graph, nodeNumber)].nodes) {
            const linkIndex = searchNodeIndex(graph, link);
            if (!graph[linkIndex].visited) {
                graph[linkIndex].visited = true;
                DFS(graph, link);
            }
        }

        return graph;
    };

    let componentAmount = 0;
    for (const node of graph) {
        if (node.visited === false) {
            componentAmount++;
            DFS(graph, node.value);
        }
    }

    //graph update to false in all object
    cleanGraph(graph);

    return componentAmount;
};

const getThirdAnswer = () => {
    const BFS = (node) => {
        const queue = [];
        node.high = 0;
        node.visited = true;
        queue.push(node);

        while (queue.length !== 0) {
            let element = queue.shift();
            for (let el of element.nodes) {
                const nodeIndex = searchNodeIndex(graph, el);
                if (!graph[nodeIndex].visited) {
                    graph[nodeIndex].high = element.high + 1;
                    if (element.high + 1 > maxLength) {
                        maxLength = element.high + 1;
                    }
                    graph[nodeIndex].visited = true;
                    queue.push(graph[nodeIndex]);
                }
            }

        }
    };

    let maxLength = 0;
    for (const node of graph) {
        cleanGraph(graph);
        BFS(node);
    }

    return maxLength;
};

const getFourthAnswer = () => {
    const graph2 = getGraphFromString(tasks[3]);
    let isSubgraph = true;
    for (let node of graph2) {
        const nodeIndex = searchNodeIndex(graph, node.value);
        if (!checkAdjNodes(graph[nodeIndex].nodes, node.nodes)) {
            isSubgraph = false;
            break;
        }
    }

    const answer = new Set();
    if (isSubgraph) {
        for (const node of graph) {
            const nodeIndexInGraph2 = searchNodeIndex(graph2, node.value);
            if (nodeIndexInGraph2 !== null) {
                for (const element of node.nodes) {
                    if (!graph2[nodeIndexInGraph2].nodes.includes(element)) {
                        answer.add(graph2[nodeIndexInGraph2].value);
                    }
                }
            }
        }

        return Array.from(answer).join(" ");
    }

    return "It is not subgraph";
};

const checkAdjNodes = (nodes1, nodes2) => {
    for (const node of nodes2) {
        if (!nodes1.includes(node)) {
            return false;
        }
    }

    return true;
};

const cleanGraph = (graph) => {
    for (let node of graph) {
        node.visited = false;
        node.high = -1;
    }
};

const searchNodeIndex = (graph, nodeNumber) => {
    for (let i = 0; i < graph.length; i++) {
        if (graph[i].value === nodeNumber) {
            return i;
        }
    }

    return null;
};

const startTasks1 = () => {
    cleanFile();

    tasks = getTasks1("test3.in");
    graph = getGraphFromFile("test1.graph");

    writeToFile(stripIndent`
        #1.1
        ${getFirstAnswer()}
        #1.2
        ${getSecondAnswer()}
        #1.3
        ${getThirdAnswer()}
        #1.4
        ${getFourthAnswer()}`);
};

module.exports.startTasks1 = startTasks1;

"use strict";

const main = () => {
    const A = [
        [4, 1, 1],
        [1, 6, 1],
        [1, 1, 8]
    ];
    const b = [
        6,
        8,
        10
    ];

    const N = 10;

    console.log("Gauss method");
    console.log(gauss(generateA(N), generateB(N)));
    console.log();
    console.log("Zeidel method");
    console.log(zeidel(generateA(10), generateB(10)));
};

const gauss = (A, b) => {
    const N = A.length;
    const result = [];
    const AB = generateAB(A, b);

    for (let k = 0; k < N; k++) {
        // delim na pervii nenulevoi element v stroke
        for (let i = 0; i < N; i++) {
            if (AB[i][k] !== 0) {
                const one = AB[i][k];
                for (let j = 0; j < N + 1; j++) {
                    AB[i][j] /= one;
                }
            }
        }
        // vichitaem is vseh strok i-u stroky -> diagonalnaya matrica
        for (let i = k; i < N - 1; i++) {
            for (let j = 0; j < N + 1; j++) {
                if (AB[i + 1][j] !== 0) {
                    AB[i + 1][j] -= AB[k][j];
                }
            }
        }
    }


    for (let k = N - 1; k >= 0; k--) {
       //delim na posledniy element -> stolbec edenic
        for (let i = k; i >= 0; i--) {
            if (AB[i][k] !== 0) {
                const one = AB[i][k];
                for (let j = 0; j < N + 1; j++) {
                    AB[i][j] /= one;
                }
            }
        }
        // vicitaem poslednuu stroku is predidushih
        for (let i = k; i > 0; i--) {
            for (let j = 0; j < N + 1; j++) {
                if (AB[i - 1][j] !== 0) {
                    AB[i - 1][j] -= AB[k][j];
                }
            }
        }
    }

    for (let i = 0; i < N; i++) {
        result[i] = Number(AB[i][N].toFixed(6));
    }

    return result;
};

const zeidel = (A, b) => {
    const N = A.length;
    const E = 0.0001;
    const An = multMatrixMatrix(trans(A), A);
    const Bn = multMatrixRow(trans(A), b);
    for (let i = 0; i < N; i++) {
        b[i] = Bn[i];
    }
    for (let i = 0; i < N; i++) {
        for (let j = 0; j < N; j++) {
            A[i][j] = An[i][j];
        }
    }

    let X1 = [];
    let X2 = [];
    for (let i = 0; i < N; i++)
    {
        X1[i] = 0;
        X2[i] = 0;
    }
    for (let i = 0; i < N; i++)
    {
        if (A[i][i] !== 0) {
            const diag = A[i][i];
            for (let j = 0; j < N; j++) {
                A[i][j] /= diag;
            }
            b[i] /= diag;
        }
    }

    do {
        for (let i = 0; i < N; i++) {
            X1[i] = X2[i];
        }
        X2 = iterZeidel(A, b, X1);
    } while (Math.abs(norm(A) / (1 - norm(A)) * norm2(X1, X2)) > E);

    for (let i = 0; i < X2.length; i++) {
        X2[i] = Number(X2[i].toFixed(6));
    }

    return X2;
};

const generateA = (N) => {
    const A = [];
    const mu = 0.0002;
    for (let i = 0; i < N; i++) {
        A.push([]);
    }
    for (let i = 0; i < N; i++) {
        for (let j = 0; j < N; j++) {
            if (i === j) {
                A[i][j] = 1 + mu;
            } else if (i > j) {
                A[i][j] = mu;
            } else {
                A[i][j] = -1 - mu;
            }
        }

    }

    return A;
};

const generateAB = (A, b) => {
    const N = A.length;
    const AB = [];
    for (let i = 0; i < N; i++) {
        AB.push([]);
        for (let j = 0; j < N; j++) {
            AB[i][j] = A[i][j];
        }
        AB[i][N] = b[i];
    }

    return AB;
};

const generateB = (N) => {
    const b = [];
    const E = 0.0001;
    for (let i = 0; i < N; i++) {
        b.push(-1);
    }
    b[N - 1] += 2 + E;

    return b;
};

const iterZeidel = (A, b, X) => {
    let N = X.length;
    const X2 = [];
    for (let i = 0; i < X.length; i++) {
        X2[i] = X[i];
    }
    for (let i = 0; i < N; i++) {
        let Asum = 0;
        for (let j = 0; j < N; j++) {
            if (i !== j) {
                Asum += A[i][j] * X2[j];
            }
        }
        X2[i] = b[i] - Asum;
    }

    return X2;
};

const trans = (A) => {
    const T = [];
    for (let i = 0; i < A.length; i++) {
        T.push([]);
    }
    for (let i = 0; i < A.length; i++) {
        for (let j = 0; j < A.length; j++) {
            T[j][i] = A[i][j];
        }
    }

    return T;
};

const norm = (A) => {
    let maxSum = 0;
    for (let j = 0; j < A[0].length; j++) {
        let sum = 0;
        for (let i = 0; i < A.length; i++) {
            sum += Math.abs(A[i][j]);
        }
        if (maxSum < sum) {
            maxSum = sum;
        }
    }

    return maxSum;
};

const norm2 = (X1, X2) => {
    let sum = 0;
    for (let i = 0; i < X1.length; i++) {
        sum += Math.pow(X1[i] - X2[i], 2);
    }

    return Math.sqrt(sum);
};

const multMatrixMatrix = (A1, A2) => {
    const M = [];

    for (let i = 0; i < A1.length; i++) {
        M.push([]);
        for (let j = 0; j < A2[0].length; j++) {
            let sum = 0;
            for (let k = 0; k < A1[0].length; k++) {
                sum += A1[i][k] * A2[k][j];
            }
            M[i][j] = sum;
        }
    }

    return M;
};

const multMatrixRow = (A1, A2) => {
    const M = [[]];

    for (let i = 0; i < A1.length; i++) {
        let sum = 0;
        for (let k = 0; k < A1[0].length; k++) {
            sum += A1[i][k] * A2[k];
        }
        M[i] = sum;
    }

    return M;
};

main();
const cTable = require("console.table");
const accuracyNumber = 8;

const task1 = () => {
    const xArray = getX(0.01, 0.06, 0.005);
    console.log(`X array: ${xArray.join(" ")}`);

    const yArray = getY(xArray);
    console.log(`Y array: ${yArray.join(" ")}`);

    const atanArray = atan(xArray);
    console.log(`Atan array: ${atanArray.join(" ")}`);

    const sinArray = sin(xArray);
    console.log(`Asin array: ${sinArray.join(" ")}`);

    const sqrtArray = sqrt(atanArray);
    console.log(`Sqrt array: ${sqrtArray.join(" ")}`);

    const seriesArray = series(sinArray, sqrtArray);
    console.log(`Series array: ${seriesArray.join(" ")}`);

    //output
    const printMatrix = [];
    for (let i = 0; i < xArray.length; i++) {
        printMatrix.push({
            x: xArray[i],
            y: yArray[i],
            series: seriesArray[i],
            difference: Math.abs(seriesArray[i] - yArray[i]).toFixed(accuracyNumber)});
    }
    console.table(printMatrix);

};

function getX(a, b, step){
    const res = [];
    for (let i = a; i < b; i += step) {
        res.push(Number(i.toFixed(3)));
    }

    return res;
}

function getY (xArray) {
    const res = [];
    for (const x of xArray) {
        res.push(Number((Math.sqrt(1 + Math.atan(6.4 * x + 1.1)) / (Math.sin(2 * x + 1.05))).toFixed(accuracyNumber)));
    }

    return res;
}

function atan(xArray) {
    const res = [];
    let tmp;

    for (const y of xArray) {
        const x = y * 6.4 + 1.1;
        const eps = 0.000065359;
        let k = 0;
        let r = 0;

        if (Math.abs(x) < 1) {
            tmp = 0;
            do {
                r = Math.pow(-1, k) * Math.pow(x, 2 * k + 1) / (2 * k + 1);
                tmp += r;
                k++;
            } while (Math.abs(r) > eps)
        } else {
            tmp = Math.PI / 2 * Math.sign(x);
            do {
                r = Math.pow(-1, k) * Math.pow(x, -2 * k - 1) / (2 * k + 1);
                tmp -= r;
                k++;
            } while (Math.abs(r) > eps)
        }

        res.push(Number(tmp.toFixed(accuracyNumber)));
    }

    return res;
}

function sin(xArray) {
    const res = [];
    let tmp;

    for (const y of xArray){
        const x = 2 * y + 1.05;
        const eps = 0.000065359;
        let k = 0;
        let r = 0;
        tmp = 0;

        do {
            r = Math.pow(-1, k) * Math.pow(x, 2 * k + 1) / factorial(2 * k + 1);
            tmp += r;
            k++;
        } while (Math.abs(r) > eps);

        res.push(Number(tmp.toFixed(accuracyNumber)));
    }

    return res;
}

function factorial(n) {
       return (n > 1) ? n * factorial(n - 1) : 1;
}

function sqrt(atanArray) {
    const res = [];
    const eps = 0.0001;
    let tmp;

    for (const y of atanArray){
        const x = y + 1;
        let a = 1;
        let b = x;
        let S = x;
        tmp = 0;

        while(Math.abs(a - b) > eps) {
            a = (a + b) / 2;
            b = S / a;
        }
        tmp = (a + b) / 2;

        res.push(Number(tmp.toFixed(5)));
    }

    return res;
}

function series(sinArray, sqrtArray) {
    const res = [];

    for (let i = 0; i < sqrtArray.length; i++) {
        res.push(Number((sqrtArray[i] / sinArray[i]).toFixed(accuracyNumber)));
    }

    return res;
}

task1();